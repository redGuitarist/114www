gitlab_rails['gitlab_email_enabled'] = true
gitlab_rails['gitlab_email_from'] = 'git@114-7.com'
gitlab_rails['gitlab_email_display_name'] = '114-7 GitLab'

gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "mail.114-7.com"
gitlab_rails['smtp_port'] = 25
gitlab_rails['smtp_user_name'] = "git@114-7.com"
gitlab_rails['smtp_password'] = "<REDACTED>"
gitlab_rails['smtp_domain'] = "114-7.com"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true

gitlab_rails['gitlab_shell_ssh_port'] = 8022

gitlab_rails['smtp_tls'] = false
