from flask import Flask, send_file, request
import io
import imgkit

app = Flask(__name__)
cache = {}


@app.route('/', methods=['GET'])
def main():
    # initialize defaults
    url = 'https://google.com'
    width = 1200
    height = 1200
    quality = 50

    # parse request
    if 'url' in request.args:
        url = request.args['url']
    if 'w' in request.args:
        width = request.args['w']
    if 'h' in request.args:
        height = request.args['h']
    if 'q' in request.args:
        quality = request.args['q']
    request_string = f'{url}-{width}-{height}-{quality}'

    # try for cache
    if request_string in cache:
       print('Cache used for: ', request_string)
       return send_file(io.BytesIO(cache[request_string]), mimetype='image/png')

    # set options
    options = {
            'quiet': '',
            'width': width,
            'height': height
            }

    # get image
    image_binary = imgkit.from_url(url, False, options=options)

    # save to cache
    cache[request_string] = image_binary

    # return image
    return send_file(io.BytesIO(image_binary), mimetype='image/png')

@app.before_request
def anyreq():
    with open("sshot.log", "a") as file:
        file.write("Serving request " + request.query_string.decode() + '\n')

if __name__ == '__main__':
    app.run()
