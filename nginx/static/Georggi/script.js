var nav = document.getElementsByClassName('header-row-menu')[0];
var btn = document.getElementsByClassName("btndrop")[0];
var menu = document.getElementsByClassName("header-row-menu")[0];

document.getElementsByClassName("btndrop")[0].addEventListener("click", handleMenuClick);

document.getElementsByClassName("container")[0].addEventListener("click", function (e) {
    if (!nav.contains(e.target) && !btn.contains(e.target)) {
        hideMobileMenu();
    }
});

window.addEventListener('resize', function (e) {
    if(menu.classList.contains('active')){
        hideMobileMenu();
    }
    if(window.outerWidth > 855){
        menu.style.display = "flex";
    }
    if(window.outerWidth <= 855){
        menu.style.display = "none";
    }
});

purgeDoubleClickers('submit-email');
purgeDoubleClickers('search-submit');
purgeDoubleClickers('submit-tab');

function purgeDoubleClickers(name){//AKA: Slow internet sucks
    document.getElementById(name).addEventListener('click', function (e) {
        e.preventDefault();
        var el = e.target;
        if(!el.disabled){
            el.disabled = true;
            el.value="Please wait.";
            setTimeout(function () {
                el.disabled = false;
                el.value="Submit.";
            }, 3000);
        }
    });
}

function openTab(evt, tabname) {
    var i, tabcontent, tablinks;

    tabcontent = document.getElementsByClassName("tab-content");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    document.getElementById(tabname).style.display = "flex";
    evt.currentTarget.className += " active";
}

function hideMobileMenu() {
    if(menu.classList.contains('active')){
        menu.className = menu.className.replace(" active", "");
        menu.style.display = "none";
    }
}

function showmobilemenu() {
    menu.style.display = "flex";
    menu.className += " active";
}

function handleMenuClick() {
    console.log('F');
    if (menu.classList.contains("active")) {
        hideMobileMenu(menu);
    } else {
        showmobilemenu(menu);
    }
}

function createNewTab(){
    var tab_name = document.forms['tab-form'].elements[0].value;
    var tab_text = document.forms['tab-form'].elements[1].value;
    document.getElementsByClassName('tab-options')[0].innerHTML += "<button class=\"tablinks\" onclick=\"openTab(event, '" + tab_name + "')\"> "+ tab_name + "</button>"
    document.getElementsByClassName('vertical-tab-row')[0].innerHTML += "<div class=\"tab-content\" id=\"" + tab_name + "\" style=\"display: none\">" + tab_text + "</div>"
}





