const title = document.getElementsByClassName('accordion-title');
const acc = document.getElementsByClassName('accordion');

function display(){
    accnum = this.getAttribute('acc');
    var active = acc[accnum].classList.contains('hidden') ? 0 : 1;
    for(var j = 0; j < acc.length; j++){
        acc[j].classList.add('hidden');
    }
    if(active == 0){
        acc[accnum].classList.remove('hidden');
    }
};

for(var i = 0; i<title.length; i++){
    acc[i].setAttribute('acc', String(i));
    title[i].setAttribute('acc', String(i));

    title[i].addEventListener('click',display);
}

