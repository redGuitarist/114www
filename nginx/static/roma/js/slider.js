function Slider(wrap){
    wrap.setAttribute('currSlide', String(0));
    wrap.setAttribute('numOFSlides', String(wrap.getElementsByTagName('li').length));

    wrap.children[2].addEventListener('click', nextSlide);
    wrap.lastElementChild.addEventListener('click', previousSlide);
}

function getSliderParams(target){
    var sliderParams = [];
    sliderParams.push( parseInt(target.parentElement.getAttribute('currSlide')) );
    sliderParams.push( parseInt(target.parentElement.getAttribute('numOFSlides')) );
    sliderParams.push( target.parentElement.children[1].children );
    return sliderParams;
}

function changeSlide(target, currSlideNum, numOFSlides, slides, step){
    slides[currSlideNum].classList.remove('showing');
    currSlideNum += step;
    currSlideNum = currSlideNum >= numOFSlides ? currSlideNum - numOFSlides : currSlideNum;
    currSlideNum = currSlideNum < 0 ? currSlideNum + numOFSlides : currSlideNum;
    target.parentElement.setAttribute('currSlide', String(currSlideNum));
    slides[currSlideNum].classList.add('showing');
}

function nextSlide(){
    sliderParams = getSliderParams(this);
    //ChangeSlide(target, currSlideNum, numOFSlides, slides, step)
    changeSlide(this, sliderParams[0], sliderParams[1], sliderParams[2], 1);
}

function previousSlide(){
    sliderParams = getSliderParams(this);
    changeSlide(this, sliderParams[0], sliderParams[1], sliderParams[2], -1);
}

const sliderModule = document.getElementsByClassName('slider-wrap');
for(var i = 0; i < sliderModule.length; i++){
    Slider(sliderModule[i]);
}
