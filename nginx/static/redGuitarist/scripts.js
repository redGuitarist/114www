//    POPUP AND MOBILE MENU



const popup_shade = document.getElementById('login_popup_shade');
const popup_window = document.getElementById('login_popup');
const nav = document.getElementById('menu');
const menu_btn = document.getElementById('menu_btn');

// click listener for popup open
document.getElementById('popupopen_button').addEventListener('click', function () {
  popup_shade.classList.remove('popup_disabled');
  popup_window.classList.remove('popup_disabled');
});

// click listener for popup close
document.getElementById('popupclose_button').addEventListener('click', function () {
  popup_shade.classList.add('popup_disabled');
  popup_window.classList.add('popup_disabled');
});

// click listener for mobile menu open/close
menu_btn.addEventListener('click', function () {
  nav.classList.toggle('nav_shown');
});

// click listener for mobile menu hiding by clicking outside
document.addEventListener('click', function (e) {
  if (!menu_btn.contains(e.target) && !nav.contains(e.target))
    nav.classList.remove('nav_shown');
});



//    GALLERY GRID



const grids = document.getElementsByClassName('gallery_grid');
const image_popup = document.getElementById('image_popup');
var swapping = new Array;
var swap_node = new Array;


// swaps two given nodes, saving all properties and listeners
function swap_nodes(a, b) {
  var t = a.parentNode.insertBefore(document.createTextNode(''), a);
  b.parentNode.insertBefore(a, b);
  t.parentNode.insertBefore(b, t);
  t.parentNode.removeChild(t);
}

// swap clicked images
function do_swap(e) {
  const grid_id = e.target.parentNode.getAttribute('data-grid-id'); // get number of current grid
  if (!swapping[grid_id]) { // remember chosen node
    swapping[grid_id] = true;
    swap_node[grid_id] = e.target.parentNode;
    swap_node[grid_id].firstElementChild.classList.add('grid_image_chosen');
  }
  else {  // perform swap
    swapping[grid_id] = false;
    swap_node[grid_id].firstElementChild.classList.remove('grid_image_chosen');
    swap_nodes(e.target.parentNode, swap_node[grid_id]);
  }
}

// pop up doubleclicked image
function pop_up(e) {
  image_popup.removeChild(image_popup.lastElementChild);
  const image = e.target.cloneNode(true);
  image.classList.add('expanded_image');
  image_popup.appendChild(image);
  popup_shade.classList.remove('popup_disabled');
  image_popup.classList.remove('popup_disabled');
}

for (var i = 0; i < grids.length; i++) {  // for each gallery grid in a document
  for (var j = 0; j < grids[i].children.length; j++) {  // for each child node in gallery grid
    grids[i].children[j].setAttribute('data-grid-id', i); // remember the grid number
    grids[i].children[j].addEventListener('click', do_swap); // add listener for swapping
    grids[i].children[j].addEventListener('dblclick', pop_up); // add listener for popping up
    swap_node.push(null);
    swapping.push(false);
  }
}

// image popup close
document.getElementById('imageclose_button').addEventListener('click', function () {
  image_popup.classList.add('popup_disabled');
  popup_shade.classList.add('popup_disabled');
});