#!/usr/bin/env bash

mkdir -p data/gitlab
mkdir -p data/grafana
chown -R 472:472 data/grafana
mkdir -p data/jenkins
chown -R 1000:1000 data/jenkins
mkdir -p data/prometheus
chown -R 65534:65534 data/prometheus