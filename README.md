## 114www - Our web service

Each directory holds code and configs for particular service.

### nginx
Primary webserver, serves static files and acts as reverse proxy for other services.   
**Path:** /

### get-sshot
REST screenshot utility, used by static sites.   
**Path:** /get_sshot

### zabbix
System monitoring tool (server)   
**Path:** /zabbix
